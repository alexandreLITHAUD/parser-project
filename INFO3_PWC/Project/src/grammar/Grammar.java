package grammar;

import java.util.ArrayList;
import java.util.LinkedList;

import shared.OPTIONS;
import shared.TRACING;

import export.GRM;
import finite_automata.DFA;
import finite_automata.FSA;
import finite_automata.Run;
import finite_automata.State;
import finite_automata.SharedStates;
import finite_automata.Symbol;
import finite_automata.Transition;
import finite_automata.SharedSymbols;

public class Grammar {

	// SHARING

	private SharedStates _all_states;
	private SharedSymbols _all_symbols;

	public State state(String name) {
		return _all_states.state(name);
	}

	public Symbol symbol(String letter) {
		return _all_symbols.symbol(letter);
	}

	// FIELDS

	private String _name;
	private ArrayList<Language> _languages;

	private FSA _fsa; // The Finite State Automaton associated to the regular grammar

	private ArrayList<Run> _runs; // executions of the FSA on given words
	
	public Grammar() {
		_all_states = new SharedStates();
		_all_symbols = new SharedSymbols();
		_languages = new ArrayList<Language>();
		_runs = new ArrayList<Run>();
		_fsa = null;
	}

	// BUILDER

	public void set_name(String name) {
		_name = name;
	}

	// =TODO,FIXME= 
	public Language language(String name) {
		//TRACING.fixme("Grammar.language: FIXME");
		// /!\ à modifier pour avoir du sharing
		
		for (Language languageList: _languages) {
			if (name.equals(languageList.name())){
				return languageList;
			}
		}
		Language language = new Language(this, name);
		_languages.add(language);
		return language;
	}

	// =TODO=
	public void add_run(Run run) {
		//On tente des choses
		this._runs.add(run);
	}
	
	// =given= EXPORT
	
	// =TODO= 
	public String to_GRM() {
	
		String content= new String();
		
		for (Language language : _languages) {// on boucle chaque langage de la liste
		 
			LinkedList<String> listprod=new LinkedList<String>();
			
			for(int i=0;i<language.production().size();i++){ // on boucle sur la liste de production du langage
				Production production= language.production().get(i); // on récupère la i-ème production du langage
				String prod= GRM.symbol(production.symbol.name()); // on ajoute le symbole au String prod
				// on vérifie si le symbole est suivie d'un état
				// si oui on ajoute l'état à prod
				if (production.language!=null) {	 
					prod+="."+production.language.name();	
				}
				listprod.add(prod); // on ajoute prod à la liste de production
			}
			content+= GRM.language_def(language.name(),listprod);	// on ajoute à content chaque rule  
		}
			
		String S= GRM.grm(_name,content); // content contient l'ensemble des rules
		// ajout des différentes run 
		for(Run run:_runs) {
			S+=run.initial_state.name()+"("+run.word+");\n";
		}	
		return S;
		
	}

	// =given=
	public void as_FSA() {
		_fsa = new FSA(_all_states, _all_symbols, _name);
		for (Language language : _languages) {
			language.as_transition_of(_fsa);
		}
	}

	// =given=
	public void export() {
		// GRM 
		GRM.to_grm_file(OPTIONS.output_path, this._name, to_GRM());
		// FSA
		Language seed = null;
		if (!_languages.isEmpty())
			seed = _languages.get(0);
		this.as_FSA();
		TRACING.info("seed=" + seed.state().toString());
		this._fsa.initial_state(seed.state());
		this._fsa.export();
		DFA dfa=this._fsa.as_DFA();		
		if (dfa.is_determinitic()) {
		dfa.to_html();
		}
	}

	// =given= DEMO
	
	public void demo() {
		export();
		for (Run run : _runs) {
			
			if(this._fsa.accept(run.initial_state, run.word)) {
				System.out.println("\n" + run.initial_state.name() + "(" + run.word + ") = " + "accept");
			}
			else {
				System.out.println("\n" + run.initial_state.name() + "(" + run.word + ") = " + "reject");
			}
		}
			
	}

}
