package export;

import shared.FILE;

public class AEF {

	public static String accepting_state(String label) {
		return "(" + label + ")";
	}

	public static String initial_state(String label) {
		return newline("->" + label);
	}

	public static String epsilon_transition(String source, String target) {
		return edge(source, "", target);
	}

	public static String edge(String source, String label, String target) {
		return newline(source + " -" + label + "-> " + target);
	}

	private static String newline(String content) {
		return "\n  " + content + " ;";
	}

	public static String aef(String name, String aef_body) {
		return "AEF(" + name + "){" + aef_body + "\n}\n";
	}

	public static void to_aef_file(String path, String name, String content) {
		FILE.to_file(path, name, "aef", content);
	}

}
