package number_package;

public class OptDouble {

	boolean available;
	double value;

	OptDouble(){
		available = false;
	}

	OptDouble(double d){
		available = true;
		value = d;
	}
}
