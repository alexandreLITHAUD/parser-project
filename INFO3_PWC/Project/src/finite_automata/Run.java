/* Michaël PÉRIN, VERIMAG / Univ. Grenoble-Alpes / Polytech Grenoble, december 2021 */

package finite_automata;

public class Run {

	public State initial_state;
	public String word;
	
	public Run(State an_initial_state, String a_word) {
		initial_state = an_initial_state;
		word = a_word;
	}
}
