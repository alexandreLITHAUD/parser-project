
```Java
PARSE ::= GRAMMAR . \EOF

GRAMMAR ::= "Grammar" . "(" . IDENT . ")" . "{" . RULES . "}" . RUNS

RULES ::= RULE . RULES | epsilon 

RULE ::=  IDENT . "::=" . PRODUCTION . PRODUCTIONS

PRODUCTIONS ::= "|" . PRODUCTION . PRODUCTIONS | epsilon

PRODUCTION ::= STRING .  | STRING . IDENT

STRING ::= QUOTE . STRING . QUOTE | QUOTE . QUOTE

SYMBOL ::= LOWERCASE | UPPERCASE | DIGIT | OTHER

WORD ::= LOWERCASE*

RUNS ::= RUN . RUNS | epsilon

RUN ::= IDENT . "(" . WORD . ")" . ";"

// extensions (++) et (+++)

EXPR ::=
  | STRING
  | IDENT
  | CHOICE
  | SEQUENCE

CHOICE ::= "(" . STRING . ( "|" . STRING)* . ")"

SEQUENCE ::= EXPR . ( "." EXPR )*
```
