package export;

import java.util.Iterator;
import java.util.LinkedList;

import shared.FILE;
import shared.TRACING;;

public class GRM {

	private static String newline(String content) {
		return "\n  " + content;
	}

	public static String symbol(String string) {
		return "\"" + string + "\"";
	}

	public static String non_terminal(String string) {
		return " " + string + " ";
	}

	public static String language_def(String name, LinkedList<String> strings) {
		String res = new String("\n"+name+":=");
		for(int i=0;i<strings.size();i++) {
			res += strings.get(i);
			if (i+1<strings.size()) {
				res+="|";
			}
		}
		
		return res;
	}

	public static String choice(LinkedList<String> strings) {
		
		
		String res = strings.getFirst();
		for(int i=1;i<strings.size();i++) {
			res += "|" + strings.get(i);
		}
		return res;
	}

	public static String sequence(LinkedList<String> strings) {
		
		
		String res = strings.getFirst();
		for(int i=1;i<strings.size();i++) {
			res+=symbol(strings.get(i));
			res += ".";
		}
		return res;
	}

	public static String grm(String name, String body) {
		return "Grammar(" + name + "){" + body + "\n}\n";
	}

	public static void to_grm_file(String path, String name, String body) {
		FILE.to_file(path, name, "grm", body);
	}
}
