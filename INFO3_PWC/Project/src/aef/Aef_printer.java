package aef;

import java.util.ArrayList;

import shared.FILE;

public class Aef_printer {

	ArrayList<String> acceptings;
	
	public Aef_printer(ArrayList<String> acceptings){
		this.acceptings = acceptings;
	}

	public String state(String name) {
		if (this.acceptings.contains(name))
			return "(" + name + ")";
		else
			return name;
	}

	public String initial_state(String name) {
		return newline("->" + state(name));
	}

	public String edge(String source, String label, String target) {
		return newline( state(source) + " -" + label + "-> " + state(target) );
	}


	private String newline(String content) {
		return "\n  " + content + " ;";
	}
	
	public String aef(String name, String aef_body) {
		return "AEF(" + name + "){" + aef_body + "\n}\n";
	}

	public static void to_aef_file(String path, String name, String content) {
		FILE.to_file(path, name, "aef", content);
	}

}
