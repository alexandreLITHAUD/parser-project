package aef;

import export.DOT;

public class Transition {

	String source;
	String symbol;
	String target;

	Transition(String source, String symbol, String target) {
		this.source = source;
		this.symbol = symbol;
		this.target = target;
	}

	// EXPORT 
	
	// == export to DOT
	
	private String dot_initial_transition() {
		return DOT.edge(this.source, "", this.target, "color=blue");		
	}
	
	private String dot_epsilon_transition() {
		return DOT.edge(this.source, "&epsilon;", this.target, "color=gray, fontcolor=gray");
	}
	
	public String toDot() {
		if (symbol == null)
			return dot_initial_transition();
		else if (symbol.isEmpty()) 
			return dot_epsilon_transition();
		else
			return DOT.edge(this.source, this.symbol, this.target, "");
	}
	
	// == export to AEF, using the visitor pattern
	
	public String accept(Aef_printer printer) {
		if (symbol == null)
			return printer.initial_state(this.target);		
		else 
			return printer.edge(this.source, this.symbol, this.target);
	}

}
