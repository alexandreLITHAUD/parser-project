/* Michaël PÉRIN, VERIMAG / Univ. Grenoble-Alpes / Polytech Grenoble, december 2021 */

package finite_automata;

import shared.TRACING;

public class State {

	// OPTIONS

	static final boolean SHOW_NUMBER = true;
	static final boolean SHOW_NAME = true;
	static final boolean SHOW_STATUS = true;

	// FIELDS

	String _name;
	int _id;

	public boolean is_accepting;
	public boolean has_incoming_edges;
	public boolean has_outgoing_edges;

	public State(String name, int id) {
		_name = name;
		_id = id;
		is_accepting = false;
		has_incoming_edges = false;
		has_outgoing_edges = false;
		TRACING.trace("new State(\"" + _name + "\", " + _id + ");");
	}

	// ACCESS TO PRIVATE FIELDS

	public String name() {
		return _name;
	}

	public int id() {
		return _id;
	}

	// EXPORT TO STRING

	public String toString() {
		String label = new String();
		if (SHOW_NUMBER)
			label += String.format("%d", _id);
		if (SHOW_NUMBER && SHOW_NAME)
			label += ":";
		if (SHOW_NAME)
			label += _name;
		if (SHOW_STATUS && is_accepting)
			label += "^a";
		return label;
	}

}
