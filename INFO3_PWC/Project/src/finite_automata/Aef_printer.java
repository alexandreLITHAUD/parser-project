// TODO sur le modèle de Dot_printer.java

package finite_automata;

import shared.TRACING;
import export.AEF;

public class Aef_printer {

	// OPTIONS
	
	private FSA _fsa;
	
	static boolean SHOW_HOLE_STATE = true;
	static boolean SHOW_UNREACHABLE_STATE = true;

	static boolean SHOW_STATE_NUMBER = false;
	static boolean SHOW_STATE_NAME = true;
	
	public Aef_printer(FSA fsa){
		this._fsa = fsa;
	}
	
	// EXPORT
	private String printTransition() {
		
		String body = new String();
		
		for(Transition t: this._fsa.transitions()) {
			
			String source = new String();
			String target = new String();
			
			if(t.source.is_accepting == false) {
				source = t.source.name();
			}
			else {
				source = AEF.accepting_state(t.source.name());
			}
			
			if(t.target.is_accepting == false) {
				target = t.target.name();
			}
			else {
				target = AEF.accepting_state(t.target.name());
			}
			
			if(t.symbol.is_epsilon()) {
				body += AEF.epsilon_transition(source, target);
			}
			else {
				body += AEF.edge(source,t.symbol.name(),target);
			}
		}
		
		return body;
	}
	
	public String toString() {
		//TRACING.fixme("Aef_printer.toString : TODO");
		
		String body = new String();
		for(State st:this._fsa.initials()) {
			
			if(st.is_accepting) {
				body += AEF.initial_state(AEF.accepting_state(st.name()));
			}
			else {
				body += AEF.initial_state(st.name());
			}
			
		}
		
		body += printTransition();
		
		String res = AEF.aef(this._fsa.name() + "_FSA" ,body);
		
		return res;
	}
}
