/* Michaël PÉRIN, VERIMAG / Univ. Grenoble-Alpes / Polytech Grenoble, december 2021 */

package finite_automata;

import java.util.ArrayList;
import java.util.Iterator;

import shared.TRACING;

public class SharedStates implements Iterator<State>, Iterable<State> {

	// SHARING PRINCIPLE:
	// Already seen states are recorded in a list _deja_vu.
	// The methods state(name) and ith(int) give the SAME UNIQUE state object for
	// the same name or state id.

	public ArrayList<State> set;

	// CONSTANT PREDIFINED STATES

	public static State HOLE = new State("HOLE", 0);
	public static State EXIT = new State("EXIT", 1);

	public SharedStates() {
		set = new ArrayList<State>();
		set.add(HOLE);
		HOLE.is_accepting = false;
		set.add(EXIT);
		EXIT.is_accepting = true;
	}

	public int size() {
		return set.size();
	}

	// SHARING = FINDING THE UNIQUE REPRESENTATIVE

	public State ith(int i) {
		return set.get(i);
	}

	public State state(String name) {
		TRACING.trace("SharedState.state:");
		for (State state : set) {
			if (state.name().equals(name)) {
				TRACING.trace("existing state=" + state.toString());
				return state;
			}
		}
		// not found ==> make/add a new state to the list
		return new_state(name);
	}

	// NEW STATE

	private State new_state(String name) {
		int new_id = size();
		State new_state = new State(name, new_id);
		set.add(new_state);
		return new_state;
	}

	// EXPORT

	public String toString() {
		String out = new String();
		for (State state : set)
			out += state.toString() + ", ";
		return out;
	}

	// FRESH ANONYMOUS STATE

	public State fresh_state() {
		return new_state("");
	}

	// ITERATOR

	private Iterator<State> _iterator;

	public Iterator<State> iterator() {
		_iterator = set.iterator();
		return _iterator;
	}

	public boolean hasNext() {
		return _iterator.hasNext();
	}

	public State next() {
		return _iterator.next();
	}

}
