package export;

import shared.FILE;

public class DOT {

	public static String comment(String string) {
		return "\n/* " + string + " */";
	}

	public static String node_style(String options) {
		return "\n node [" + options + "]";
	}

	public static String node_decl(String node_id, String label, String options) {
		return "\n" + node(node_id) + options(label(label) + options) + ";" ;
	}

	public static String edge_style(String options) {
		return "\n edge [" + options + "]";
	}

	public static String edge(String source, String label, String target, String options) {
		return "\n" + node(source) + " -> " + node(target) + options(label(label) + options);
	}

	private static String node(String node_id) {
		return protect(node_id);
	}

	private static String protect(String string) {
		return "\"" + string + "\"";
	}

	private static String label(String label) {
		if (label == null)
			return "";
		else
			return "label=" + protect(label) + "," ;
	}

	private static String options(String options) {
		if (!options.isEmpty())
			return " [" + options + "]";
		else
			return "";
	}

	private static String more_options(String options) {
		if (options.isEmpty())
			return "";
		else
			return ", " + options;
	}

	public static String dot(String name, String dot_body) {
		String options = "/* HORIZONTAL */ rankdir=LR;";
		return "digraph " + name + "{" + options + "\n" + dot_body + "\n}\n";
	}

	public static void to_dot_file(String path, String name, String content) {
		FILE.to_file(path, name, "dot", content);
	}
}
