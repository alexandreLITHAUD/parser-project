// TODO sur le modèle de SharedStates

package finite_automata;

import java.util.ArrayList;

import shared.TRACING;

public class SharedSymbols {

	// SHARING PRINCIPLE:
	// Already seen states are recorded in a list _deja_vu.
	// The methods state(name) and ith(int) give the SAME UNIQUE state object for
	// the same name or state id.

	public ArrayList<Symbol> set;

	public static final Symbol EPSILON = new Symbol("", 0);

	public SharedSymbols() {
		set = new ArrayList<Symbol>();
		set.add(EPSILON);
	}

	public int size() {
		return set.size();
	}

	// SHARING = FINDING THE UNIQUE REPRESENTATIVE

	public Symbol ith(int i) {
		return set.get(i);
	}

	public Symbol symbol(String name) {
		TRACING.trace("SharedSymbol.symbol");
		for(Symbol sym : set) {
			if(sym.name().equals(name)){
				TRACING.trace("existing symbol=" + sym.toString());
				return sym;
			}
		}
		return new_symbol(name);
	}
	
	private Symbol new_symbol(String name) {
		int sym_id = this.size();
		Symbol s = new Symbol(name, sym_id);
		this.set.add(s);
		return s;
	}

}
